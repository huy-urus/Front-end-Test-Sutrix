$(function(){
    //add button next, prev
    $('.slide-item').prepend('<ul/>');
    $('.slide-item ul').prepend('<li id="btn-slide-left"></li>');
    $('.slide-item ul').prepend('<li id="btn-slide-right"></li>');
    
    //slide number
    var slide_num = $('.slide-img').length;

    $('.slide-content').prepend('<ul/>');

    for(i=0; i<slide_num; i++){
        $('.slide-content ul').prepend('<li class="control-box"></li>');
    }

    //$('.control-box:first-child').addClass('focus');

    var slide=0;

    $('.control-box').addClass('focus');

    //Control box to swaps slide
    $('.control-box').click(function(){
        slide = $('.control-box').index(this);
        $('.slide-img').stop().animate({
            opacity:0
        });
        $('.slide-img:eq(' + slide + ')').stop().animate({
            opacity: 1
        });
        $('.slide-cont').stop().animate({
            opacity:0
        });
        $('.slide-cont:eq(' + slide + ')').stop().animate({
            opacity: 1
        });
        $('.control-box').removeClass('focus');
        $(this).addClass('focus');
    })

    //Show slide, auto swaps slide
    function setimgfocus(){
        $('.control-box').removeClass('focus');
        $('.control-box:eq(' + slide + ')').addClass('focus');
        $('.slide-img').stop().animate({
            opacity: 0
        });
        $('.slide-img:eq(' + slide + ')').stop().animate({
            opacity: 1
        });
        $('.slide-cont').stop().animate({
            opacity: 0
        });
        $('.slide-cont:eq(' + slide + ')').stop().animate({
            opacity: 1
        });
    }

    function slide_swap(){
        if(slide==0){
            slide=slide_num;
        }
        slide--;
        setimgfocus();
    }

    start_swap();
    function start_swap(){
        time = 1500;
        play = setInterval(slide_swap, time);
    };

    $('.slideshow').hover(function(){
        clearInterval(play);
    }, function(){
        start_swap();
    });

    //Button next, prev
    $('#btn-slide-right').click(function(){
        slide--;
        if(slide==-1){
            slide=slide_num-1;
        };
        setimgfocus();
    });

    $('#btn-slide-left').click(function(){
        slide++;
        if(slide==slide_num){
            slide=0;
        }
        setimgfocus();
    });
})

//mini slide
$(function(){
    var slide_num = $('.mini-slide-img').length;

    $('.mini-slide-item').prepend('<ul/>');

    for(i=0; i<slide_num; i++){
        $('.mini-slide-item ul').prepend('<li class="mini-control-box"></li>');
    }

    var mini_slide=0;

    $('.mini-control-box').addClass('focus');

    //Control box to swaps slide
    $('.mini-control-box').click(function(){
        slide = $('.mini-control-box').index(this);
        $('.mini-slide-img').stop().animate({
            opacity:0
        });
        $('.mini-slide-img:eq(' + slide + ')').stop().animate({
            opacity: 1
        });
        $('.mini-control-box').removeClass('focus');
        $(this).addClass('focus');
    })

    //Show slide, auto swaps slide
    function mini_setimgfocus(){
        $('.mini-control-box').removeClass('focus');
        $('.mini-control-box:eq(' + mini_slide + ')').addClass('focus');
        $('.mini-slide-img').stop().animate({
            opacity: 0
        });
        $('.mini-slide-img:eq(' + mini_slide + ')').stop().animate({
            opacity: 1
        });
    }

    function mini_slide_swap(){
        if(mini_slide==0){
            mini_slide=slide_num;
        }
        mini_slide--;
        mini_setimgfocus();
    }

    mini_start_swap();
    function mini_start_swap(){
        time_ = 1500;
        play_ = setInterval(mini_slide_swap, time_);
    };

    $('.mini-slide').hover(function(){
        clearInterval(play_);
    }, function(){
        mini_start_swap();
    });
})

//medium-slide
$(function(){
    //
    $('.medium-slide-item').prepend('<ul/>');
    $('.medium-slide-item ul').prepend('<li id="btn-med-slide-left"></li>');
    $('.medium-slide-item ul').prepend('<li id="btn-med-slide-right"></li>');
    //slide number
    var slide_num = $('.slide-img').length;

    var med_slide=0;

    //Show slide, auto swaps slide
    function setimgfocus(){
        $('.medium-slide-img').stop().animate({
            opacity: 0
        });
        $('.medium-slide-img:eq(' + med_slide + ')').stop().animate({
            opacity: 1
        });
    }

    //Button next, prev
    $('#btn-med-slide-right').click(function(){
        med_slide--;
        if(med_slide==-1){
            med_slide=slide_num-1;
        };
        setimgfocus();
    });

    $('#btn-med-slide-left').click(function(){
        med_slide++;
        if(med_slide==slide_num){
            med_slide=0;
        }
        setimgfocus();
    });
})

//height left content = height right content
$(function(){
    var lheight = $('.left-content').outerHeight();
    $('.right-content').css('height', lheight)
})

$(function(){
    var msheight = $('.top-news').outerHeight(true) + $('.old-news').outerHeight();
    $('.medium-slide').css('height', msheight)
})