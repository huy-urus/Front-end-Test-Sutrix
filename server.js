var express = require('express');
var path = require('path');
var app = express();

app.set('view engine', 'pug');
app.set('views', __dirname + '/views');
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res) {
    res.render('frontend-test');
});

app.get('/slides', function(req, res){
    res.render('ssss');
});

var port = 9000;
app.listen(port, function(){
    console.log('Server started on port ' + port);
});